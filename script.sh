#!/bin/bash
 
# Redirect stdout and stderr to a log file
LOGFILE="/repos/source/update_and_build.log"
exec &> >(tee -a "$LOGFILE")
 
# Function to log messages with timestamps
log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1"
}
 
log "Script started."
 
# Change to the plugins directory
log "Changing to the plugins directory."
cd Documents/streamlit_test/gocd_poc/plugins || { log "Error: Could not change directory to /opt/backstage/plugins"; exit 1; }
 
# Pull the latest changes from the development branch
log "Pulling latest changes from the development branch."
git pull plugin main || { log "Error: git pull failed"; exit 1; }
 
# Copy all the content to /opt/backstage/rwbackstage/plugins
log "Copying plugins to /opt/backstage/rwbackstage/plugins."
cp -R /repos/source/* /repos/dest/ || { log "Error: Copying plugins failed"; exit 1; }
 
# Changing to Destination directory
cd /repos/dest || { log "Error: Unable to change directory"; exit 1; }

# Commiting changes
git add --all
git commit -m "Adding changes (date)" 

# Pushin code
git push dest main || { log "Error: Unable to push code"; exit 1; }

log "Script completed successfully."
