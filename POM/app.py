import requests
import streamlit as st
import base64
from streamlit_lottie import st_lottie
from PIL import Image

# Find more emojis here: https://www.webfx.com/tools/emoji-cheat-sheet/
st.set_page_config(page_title="My Web Page", page_icon=":tada:", layout="wide")

def load_lottie_url(url):
    r = requests.get(url)
    if r.status_code != 200:
        return None
    return r.json()


# --- Use local CSS ----
def local_css(file_name):
    with open(file_name) as f:
        st.markdown(f"<style>{f.read()}</style>", unsafe_allow_html=True)

local_css("style/style.css")


# ---- Load Assets ----
lottie_coding = load_lottie_url("https://lottie.host/03a81ff9-88b7-4100-b95c-1b80e495b65f/WzYUoL6CJl.json")
img_animated = Image.open("images/mr_robot1.gif")
img_lottie = Image.open("images/image2.png")


# ---- Header Section ----
with st.container():
    st.header("Hi this is my first website :wave:")
    st.title("Just a Tech...")
    st.write("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")

# ---- What I Do ----
with st.container():
    st.write("---")
    left_column, right_column = st.columns(2)
    with left_column:
        st.header("What I Do")
        st.write("##")
        st.write(
            """
                 Duis scelerisque, nisl sed feugiat sodales, massa magna laoreet elit,
        vitae mattis sapien libero a ligula. Vestibulum eget volutpat erat. 
        Mauris nec magna non ex hendrerit efficitur nec vel lacus. 
        Etiam a ex vitae ipsum volutpat elementum. Integer a tellus vitae odio porttitor lobortis. 
        Donec non pharetra ipsum. Nam pulvinar condimentum sem.
        """
        )
        st.write("[YouTuBe Channel >](https://youtube.com/)")

        with right_column:
            st_lottie(lottie_coding, height= 300, key="coding")

# ---- Projects ----
# with st.container():
#     st.write("----")
#     st.title("My Projects")
#     st.write("##")
#     st.header("Project #1")
#     st.write("##")
#     image_column, text_column = st.columns((1, 2))
#     with image_column:
#         file_ = open("images/mr_robot1.gif", "rb")
#         contents = file_.read()
#         data_url = base64.b64encode(contents).decode("utf-8")
#         file_.close()
#         st.markdown(
#             f'<img src="data:image/gif;base64,{data_url}" alt="animated image">',
#             unsafe_allow_html=True,
#         )
#         # st.image(img_animated)
#         # st.image(img_lottie)
#         # st.write("---- insert image ----")
#     with text_column:
#         st.subheader("Integrate Lottie Animations Inside our Streamlit web-app")
#         st.write(
#             """Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
#         Aenean bibendum lacus felis, id vulputate est imperdiet et. Ut ut velit non libero bibendum rhoncus. 
#         Nulla nec bibendum tellus, sit amet mollis elit. Pellentesque ultrices venenatis libero, 
#         porta facilisis eros blandit ac. Quisque vehicula ante libero, et ullamcorper lectus pulvinar sit amet. 
#         Nam dolor dui, fermentum in posuere non, lacinia id magna. Phasellus ut volutpat velit. 
#         Cras lobortis nec ex id auctor. """
#         )
#         st.markdown('[Watch the video ->](https://youtube.com)')
# 
# with st.container():
#     st.write("----")
#     st.header("Project #2")
#     st.write("##")
#     image_column, text_column = st.columns((1, 2))
#     with image_column:
#         st.image(img_lottie)
#         # st.image(img_lottie)
#         # st.write("---- insert image ----")
#     with text_column:
#         st.subheader("Integrate Lottie Animations Inside our Streamlit web-app")
#         st.write(
#             """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sit amet tristique eros, at gravida dui. 
#             Cras lobortis nunc in libero vestibulum lacinia. Ut congue augue in turpis vestibulum, 
#             sed dignissim libero rutrum. Morbi varius augue ac lacus facilisis, a pellentesque nibh auctor. 
#             Nullam vitae quam in leo lacinia varius. Morbi at egestas libero. Etiam ac pulvinar leo, sed sagittis nunc. 
#             Cras euismod nec nisi vel imperdiet. Maecenas fermentum ipsum in ex accumsan, eu volutpat urna sagittis. 
#             In malesuada in enim at dapibus. In non elementum lectus. Quisque fringilla maximus suscipit. 
#             Praesent fermentum lectus mi, vel maximus mi maximus vel. Nulla a viverra tortor. 
#             Fusce aliquam ut mi vel suscipit. Suspendisse potenti. """
#         )
#         st.markdown('[Watch the video ->](https://youtube.com)')

# ---- Contact ----
with st.container():
    st.write("----")
    st.header("Get in touch with me...")
    st.write("##")
    contact_form = """
    <form action="https://formsubmit.co/willie.gutierrez@rulesware.com" method="POST">
        <input type="hidden" name="_captcha" value="false">
        <input type="text" name="name" required>
        <input type="email" name="email" required>
        <textarea name="message" placeholder="Your message here..." required></textarea>
        <button type="submit">Send</button>
    </form>
    """
    left_column, right_column = st.columns(2)
    with left_column:
        st.markdown(contact_form, unsafe_allow_html=True)

    with right_column:
        st.empty




