import streamlit as st

# Define functions for each page
def login_page():
  st.title("Login Page")
  username = st.text_input("Username")
  password = st.text_input("Password", type="password")
  if st.button("Login"):
    # Check username and password
    if username == "admin" and password == "admin1":
      st.success("Login successful!")
      st.session_state.logged_in = True
      st.session_state.username = username
      # Redirect to Home page after successful login
      st.experimental_rerun()
    else:
      st.error("Invalid username or password")

def home_page():
  st.title("Home Page")
  st.write("Welcome to the Home Page!")
  st.write("You are logged in as:", st.session_state.username)

def projects_page():
  st.title("Projects Page")
  st.write("Welcome to the Projects Page!")
  st.write("You are logged in as:", st.session_state.username)

def contact_page():
  st.title("Contact Page")
  st.write("Welcome to the Contact Page!")
  st.write("You are logged in as:", st.session_state.username)

# Main function to manage page navigation
def main():
  st.sidebar.title("Navigation")
  page = st.sidebar.radio("Go to", ["Login", "Home", "Projects", "Contact"])

  # Check login status before displaying sidebar options other than Login
  if st.session_state.get("logged_in", False):
    # del st.sidebar.options[0]  # Remove Login option from sidebar
    del st.sidebar.options[0]  # Remove Login option from sidebar

  if page == "Login":
    login_page()
  elif page == "Home":
    home_page()
  elif page == "Projects":
    if st.session_state.get("logged_in"):
      projects_page()
    else:
      st.write("You need to login first.")
  elif page == "Contact":
    if st.session_state.get("logged_in"):
      contact_page()
    else:
      st.write("You need to login first.")

if __name__ == "__main__":
  main()