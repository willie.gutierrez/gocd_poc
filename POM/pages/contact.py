import requests
import streamlit as st
import base64
from streamlit_lottie import st_lottie
from PIL import Image

# Find more emojis here: https://www.webfx.com/tools/emoji-cheat-sheet/
st.set_page_config(page_title="My Web Page", page_icon=":tada:", layout="wide")

def load_lottie_url(url):
    r = requests.get(url)
    if r.status_code != 200:
        return None
    return r.json()


# --- Use local CSS ----
def local_css(file_name):
    with open(file_name) as f:
        st.markdown(f"<style>{f.read()}</style>", unsafe_allow_html=True)


local_css("style/style.css")


# ---- Load Assets ----
lottie_coding = load_lottie_url("https://lottie.host/03a81ff9-88b7-4100-b95c-1b80e495b65f/WzYUoL6CJl.json")
img_animated = Image.open("images/mr_robot1.gif")
img_lottie = Image.open("images/image2.png")


# ---- Header Section ----
with st.container():
    st.header("Hi this is my first website :wave:")
    st.title("Just a Tech...")
    st.write("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")

# ---- What I Do ----
with st.container():
    st.write("---")
    left_column, right_column = st.columns(2)
    with left_column:
        st.header("What I Do")
        st.write("##")
        st.write(
            """
                 Duis scelerisque, nisl sed feugiat sodales, massa magna laoreet elit,
        vitae mattis sapien libero a ligula. Vestibulum eget volutpat erat. 
        Mauris nec magna non ex hendrerit efficitur nec vel lacus. 
        Etiam a ex vitae ipsum volutpat elementum. Integer a tellus vitae odio porttitor lobortis. 
        Donec non pharetra ipsum. Nam pulvinar condimentum sem.
        """
        )
        st.write("[YouTuBe Channel >](https://youtube.com/)")

        with right_column:
            st_lottie(lottie_coding, height= 300, key="coding")



# ---- Contact ----
with st.container():
    st.write("----")
    st.header("Get in touch with me...")
    st.write("##")
    contact_form = """
    <form action="https://formsubmit.co/willie.gutierrez@rulesware.com" method="POST">
        <input type="hidden" name="_captcha" value="false">
        <input type="text" name="name" required>
        <input type="email" name="email" required>
        <textarea name="message" placeholder="Your message here..." required></textarea>
        <button type="submit">Send</button>
    </form>
    """
    left_column, right_column = st.columns(2)
    with left_column:
        st.markdown(contact_form, unsafe_allow_html=True)

    with right_column:
        st.empty




