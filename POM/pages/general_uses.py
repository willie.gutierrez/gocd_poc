import requests
import streamlit as st
import base64
from streamlit_lottie import st_lottie
from PIL import Image


# --- Use local CSS ----
def local_css(file_name):
    with open(file_name) as f:
        st.markdown(f"<style>{f.read()}</style>", unsafe_allow_html=True)


local_css("style/style.css")


# ---- Load Assets ----
def load_lottie_url(url):
    r = requests.get(url)
    if r.status_code != 200:
        return None
    return r.json()


lottie_coding = load_lottie_url("https://lottie.host/03a81ff9-88b7-4100-b95c-1b80e495b65f/WzYUoL6CJl.json")
img_animated = Image.open("images/mr_robot1.gif")
img_lottie = Image.open("images/image2.png")

