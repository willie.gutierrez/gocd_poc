# import goCD
import gocd

pipeline = gocd.Pipeline("my_pipeline")
stage = gocd.Stage("my_stage")
job = gocd.Job("job1", script="script.sh")

stage.add_job(job)
pipeline.add_stage(stage)

gocd.publish_pipeline(pipeline)
# goCD.publish_pipeline(pipeline)


import pymaven

# Create a Maven client
client = pymaven.MavenClient()

# Load the POM file
pom = client.load_pom("pom.xml")

# Get the dependencies
dependencies = pom.get_dependencies()

# Install the dependencies
client.install_dependencies(dependencies)

# Run the Python code
import numpy as np
from sklearn import datasets

# Use the dependencies
iris = datasets.load_iris()
print(iris.data.shape)